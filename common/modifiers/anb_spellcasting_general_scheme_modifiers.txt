﻿general_spellcasting_push_past_health = {
    icon = treatment_positive
	scheme_power = 10
	scheme_success_chance = 15
}

general_spellcasting_target_has_doubts = {
	icon = intrigue_negative
	scheme_power = -10
}

general_spellcasting_damestear_powder = {
	icon = magic_positive
	scheme_success_chance = 15
	scheme_power = 10
}

general_spellcasting_damestear_powder_other = {
	icon = magic_positive
	scheme_success_chance = 5
	scheme_power = 5
}

general_spellcasting_spellbook_advice = {
	icon = learning_positive
	scheme_success_chance = 10
	scheme_power = 10
}

general_spellcasting_spellbook_advice_deep = {
	icon = learning_positive
	scheme_success_chance = 20
	scheme_power = 10
}