﻿
remove_positive_relation_effect = {
	if = {
		limit = { has_relation_best_friend = $TARGET$ }
		remove_relation_best_friend = $TARGET$
	}
	else_if = {
		limit = { has_relation_soldier_friend = $TARGET$ }
		remove_relation_soldier_friend = $TARGET$
	}
	else_if = {
		limit = { has_relation_friend = $TARGET$ }
		remove_relation_friend = $TARGET$
	}
	else_if = {
		limit = { has_relation_potential_friend = $TARGET$ }
		remove_relation_potential_friend = $TARGET$
	}
	else_if = {
		limit = { has_relation_soulmate = $TARGET$ }
		remove_relation_soulmate = $TARGET$
	}
	else_if = {
		limit = { has_relation_lover = $TARGET$ }
		remove_relation_lover = $TARGET$
	}
	else_if = {
		limit = { has_relation_potential_lover = $TARGET$ }
		remove_relation_potential_lover = $TARGET$
	}
}