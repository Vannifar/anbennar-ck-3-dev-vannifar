﻿
# Lorentis

lorentis_0001 = { # Lorevarn II of Lorent, founder of the Lorentis house and of Lorent
	name = "Lorevarn"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti
	
	trait = race_human
	trait = education_diplomacy_3
	trait = just
	trait = impatient
	trait = ambitious
	trait = strategist
	trait = august
	
	869.8.15 = {
		birth = yes
	}
	
	895.5.8 = {
		add_spouse = caylentis_0005
	}
	
	947.1.9 = {
		death = "947.1.9"
	}
}

lorentis_0002 = { # Ruben Lorentis, died before his father
	name = "Ruben"
	dynasty_house = house_lorentis
	religion = "court_of_adean"
	culture = "lorenti"

	trait = race_human
	trait = education_martial_2
	trait = paranoid
	trait = sadistic
	trait = craven
	trait = lifestyle_hunter
	
	father = lorentis_0001
	mother = caylentis_0005
	
	887.4.9 = {
		birth = yes
	}
	
	932.5.1 = {
		death = {
			death_reason = death_hunting_accident
		}
	}
}

lorentis_0003 = { # Rewan II of Lorent
	name = "Rewan"
	dynasty_house = house_lorentis
	religion = "court_of_adean"
	culture = "lorenti"

	trait = race_human
	trait = education_learning_2
	trait = trusting
	trait = patient
	trait = gregarious
	trait = poet
	
	father = lorentis_0001
	mother = caylentis_0005
	
	890.1.20 = {
		birth = yes
	}
	
	954.11.16 = {
		death = yes
	}
}

lorentis_0004 = { # Aunnia Lorentis, married the heir of Carneter-Dameria
	name = "Aunnia"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = chaste
	trait = arbitrary
	trait = content
	
	father = lorentis_0001
	mother = caylentis_0005
	
	895.11.5 = {
		birth = yes
	}
	
	951.1.9 = {
		death = "951.1.9"
	}
}

lorentis_0005 = { # Reanna I of Lorent
	name = "ReA_nna"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti
	female = yes
	
	trait = race_human
	trait = education_stewardship_4
	trait = just
	trait = patient
	trait = stubborn
	trait = poet
	
	father = lorentis_0003
	
	920.10.10 = {
		birth = yes
		give_nickname = nick_whiterose
	}
	
	946.6.21 = {
		add_matrilineal_spouse = lorenti10000
	}
	
	975.2.28 = {
		death = "975.1.1"
	}
}

lorentis_0006 = { 
	name = "Lorevarn"
	dynasty_house = house_lorentis
	religion = "court_of_adean"
	culture = "lorenti"

	trait = race_human
	trait = education_diplomacy_2
	trait = diligent
	trait = impatient
	trait = fickle
	trait = cancer
	
	father = lorentis_0003
	
	928.12.30 = {
		birth = yes
	}
	
	950.6.1 = {
		death = {
			death_reason = death_cancer
		}
	}
}

lorenti10000 = { # Barwen of Oldport, husband of Reanna I
	name = "Barwen"
	dynasty = dynasty_oldport
	religion = court_of_adean
	culture = lorenti
	
	trait = race_human
	trait = education_martial_3
	trait = fickle
	trait = zealous
	trait = trusting
	trait = logistician
	
	918.2.23 = {
		birth = yes
	}
	
	976.1.2 = {
		death = "976.1.2"
	}
}

lorentis_0007 = { # Reanna II of Lorent
	name = "ReA_nna"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = vengeful
	trait = callous
	trait = fickle
	trait = diplomat
	
	mother = lorentis_0005
	father = lorenti10000
	
	948.5.3 = {
		birth = yes
		give_nickname = nick_ironglove
	}
	
	964.8.15 = {
		add_matrilineal_spouse = lorenti10002
	}
	
	978.10.20 = {
		add_matrilineal_spouse = dameris0005
	}
	
	990.9.15 = {
		death = "990.9.15"
	}
}

lorentis_0008 = { # Lorevarn Lorentis, brother of Reanna II, rebelled against her sister
	name = "Lorevarn"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti
	
	trait = race_human
	trait = education_martial_3
	trait = wrathful
	trait = stubborn
	trait = gregarious
	trait = flexible_leader
	trait = lifestyle_blademaster
	
	mother = lorentis_0005
	father = lorenti10000
	
	955.9.14 = {
		birth = yes
		add_trait_xp = {
			trait = lifestyle_blademaster
			value = 50
		}
	}
	
	980.9.5 = {
		death = "980.9.5"
	}
}

lorentis_0009 = { # Korvin Lorentis, brother of Reanna II, rebelled against her sister
	name = "Korvin"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti
	
	trait = race_human
	trait = education_intrigue_4
	trait = ambitious
	trait = deceitful
	trait = shy
	trait = albino
	trait = shrewd
	trait = schemer
	
	mother = lorentis_0005
	father = lorenti10000
	
	957.3.19 = {
		birth = yes
	}
	
	973.5.25 = {
		add_spouse = lorenti10001
	}
	
	980.10.10 = {
		death = {
			death_reason = death_execution
			killer = character:lorentis_0007
		}
	}
}

lorenti10001 = { # Raisenda Sil Na Loop, wife of Korvin Lorentis
	name = "Raisenda"
	dynasty_house = house_loop
	religion = court_of_adean
	culture = roilsardi
	female = yes

	trait = cynical
	trait = generous
	trait = patient
	trait = education_diplomacy_2

	trait = race_human

	948.4.23 = {
		birth = yes
	}
	
	973.5.25 = {
		add_spouse = lorentis_0009
	}
	
	1005.1.3 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

lorenti10002 = { # Roband Cardantis, first husband of Reanna II, father of Rewan III
	name = "Robald"
	dynasty = dynasty_cardantis
	religion = court_of_adean
	culture = lorenti
	
	trait = race_human
	trait = education_martial_3
	trait = compassionate
	trait = brave
	trait = arrogant
	trait = aggressive_attacker
	
	935.6.30 = {
		birth = yes
	}
	
	964.8.15 = {
		add_matrilineal_spouse = lorentis_0007
	}
	
	978.7.12 = {
		death = "978.7.12"
	}
}

lorentis_0010 = { # Laurenne Lorentis, daughter of Reanna II
	name = "Laurenne"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = brave
	trait = lustful
	trait = gregarious
	trait = lifestyle_reveler
	
	mother = lorentis_0007
	father = dameris0005
	
	981.1.16 = {
		birth = yes
	}
}

lorentis_0011 = { # Rewan III "the Thorn of the West" of Lorent
	name = "Rewan"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti

	trait = race_human
	trait = education_martial_3
	trait = stubborn
	trait = patient
	trait = brave
	
	mother = lorentis_0007
	father = lorenti10002

	969.6.21 = {
		birth = yes
	}

	990.1.1 = {
		give_nickname = nick_the_thorn_of_the_west
	}

	1014.1.4 = {
		death = {
			death_reason = death_ill
		}
	}
}

lorentis_0012 = { # Rewan Lorentis, 1st son of Rewan III
	name = "Rewan"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = lorenti
	father = lorentis_0011 #King Rewan the Thorn of the West

	trait = race_human

	990.12.5 = {
		birth = yes
	}

	991.1.13 = {
		give_nickname = nick_the_younger
	}

	1001.1.12 = {
		death = {
			death_reason = death_execution
			killer = the_sorcerer_king
		}
	}
}

lorentis_0013 = { # Kylian of Lorent
	name = "Kylian"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = "lorentish"
	father = lorentis_0011 #King Rewan the Thorn of the West
	
	trait = race_human
	trait = education_stewardship_1
	trait = greedy
	trait = zealous
	trait = content
	
	994.11.1 = {
		birth = yes
	}
	
	1015.11.1 = {	
		death = {
			death_reason = death_murder
		}
	}
}

lorentis_0014 = { # Ruben of Lorent, married Ioriel
	name = "Ruben"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = "lorentish"
	father = lorentis_0011 #King Rewan the Thorn of the West
	
	trait = race_human
	trait = education_diplomacy_4
	trait = trusting
	trait = diligent
	trait = ambitious

	1000.1.10 = {
		birth = yes
	}

	1000.2.14 = {	#Valentines
		add_matrilineal_spouse = 15 #Ioriel
	}

	1020.10.27 = {	#During Tourney of Grand Victory
		death = {
			death_reason = death_duel
			killer = 9 # Crege of Carneter
		}
	}
}

lorentis_0015 = { # Korvin Lorentis, 4th son of Rewan III, bastard
	name = "Korvin"
	dynasty_house = house_lorentis
	religion = court_of_adean
	culture = "lorentish"
	father = lorentis_0011 #King Rewan the Thorn of the West
	
	trait = education_learning_3
	trait = race_half_elf
	trait = ambitious
	trait = fickle
	trait = arrogant
	trait = bastard
	trait = magical_affinity_3


	1002.4.20 = {
		birth = yes
	}

	1020.10.31 = { #Battle of Trialmount
		death = {	
			death_reason = death_battle_of_trialmount
			killer = the_sorcerer_king	#Sorcerer-King
		}
	}
}

# Caylentis

caylentis_0001 = { # Caylen I Caylentis, king of Enteben
	name = "Caylen"
	dynasty = dynasty_caylentis
	religion = court_of_adean
	culture = entebenic
	dna = 564_caylen_caylentis
	
	diplomacy = 9
	martial = 8
	stewardship = 6
	intrigue = 2
	learning = 8
	prowess = 10
	trait = education_diplomacy_2
	trait = lustful
	trait = gregarious
	trait = generous
	trait = race_human
	trait = rakish
	trait = august
	trait = beauty_good_3
	
	798.3.8 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	855.9.12 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

caylentis_0002 = { # Caylen II Caylentis, king of Enteben
	name = "Caylen"
	dynasty = dynasty_caylentis
	religion = court_of_adean
	culture = entebenic
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = ambitious
	trait = gluttonous
	trait = beauty_good_2
	trait = open_terrain_expert
	
	father = caylentis_0001
	
	830.3.1 = {
		birth = yes
	}
	
	885.9.30 = {
		death = {
			death_reason = death_battle
		}
	}
}

caylentis_0003 = { # Arnalt, son of Caylen II, died in the same battle as him
	name = "Arnalt"
	dynasty = dynasty_caylentis
	religion = court_of_adean
	culture = entebenic
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = stubborn
	trait = trusting
	trait = beauty_good_1
	
	father = caylentis_0002
	
	858.7.16 = {
		birth = yes
	}
	
	885.9.30 = {
		death = {
			death_reason = death_battle
		}
	}
}

caylentis_0004 = { # Gawan Cayletis, king of Enteben
	name = "Gawan"
	dynasty = dynasty_caylentis
	religion = court_of_adean
	culture = entebenic
	
	trait = race_human
	trait = education_stewardship_3
	trait = fickle
	trait = content
	trait = greedy
	trait = beauty_good_1
	trait = administrator
	
	father = caylentis_0001
	
	835.10.12 = {
		birth = yes
	}
	
	865.7.15 = {
		add_spouse = roilsardi10000
	}
	
	895.12.19 = {
		death = "895.12.19"
	}
}

roilsardi10000 = { # Enora, wife of Gawan
	name = "Enora"
	# daughter of the lord-mayor of wineport
	religion = house_of_minara
	culture = lorenti
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = gregarious
	trait = impatient
	trait = diligent
	trait = physique_bad_2
	
	845.5.11 = {
		birth = yes
	}
	
	910.6.6 = {
		death = "910.6.6"
	}
}

caylentis_0005 = { # Aunnia Caylentis, daughter of Gawan, wife of Lorevarn II Lorentis
	name = "Aunnia"
	dynasty = dynasty_caylentis
	religion = court_of_adean
	culture = entebenic
	female = yes
	
	trait = race_human
	trait = education_learning_3
	trait = zealous
	trait = shy
	trait = calm
	trait = lifestyle_gardener
	
	father = caylentis_0004
	mother = roilsardi10000
	
	869.4.7 = {
		birth = yes
	}
	
	895.5.8 = {
		add_spouse = lorentis_0001
	}
	
	946.11.19 = {
		death = "946.11.19"
	}
}

20000 = { #Thayen - Duke of Lower Bloodwine
	name = "Talan"
	dna = 20000_duke_thayen
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorenti

	diplomacy = 3
	martial = 7
	stewardship = 3
	intrigue = 5
	learning = 4
	prowess = 8

	trait = arrogant
	trait = brave
	trait = wrathful
	trait = education_martial_3
	trait = physique_good_1
	trait = scarred
	trait = irritable
	trait = open_terrain_expert
	
	978.12.4 = {
		birth = yes
	}

	999.9.29 = {
		add_spouse = 20003
	}

	1020.2.6 = {
		add_spouse = 20004
		effect = {
			add_trait = human_purist
			set_variable = {
				name = racial_attitude_set
				value = yes
			}
		}
	}
}

20001 = { #Thayen's firstborn
	name = "Calas"
	dna = 20001_calas_aubergentis
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorentish
	father = 20000
	mother = 20003

	diplomacy = 5
	martial = 1
	stewardship = 4
	intrigue = 2
	learning = 8
	prowess = 0

	trait = lustful
	trait = content
	trait = trusting
	trait = education_learning_3
	trait = physique_bad_1
	trait = scarred

	1005.5.20 = {
		birth = yes
	}

	1012.5.10 = {
		effect={
			set_relation_guardian = character:20005
			set_relation_friend = character:20005
			reverse_add_opinion = {
				modifier = disappointed_opinion
				opinion = -100
				target = character:20000
			}
		}
	}

	1021.5.20 = {
		effect={
			remove_relation_guardian = character:20005
		}
	}
}

20002 = {  #Thayen's secondborn
	name = "Riualtr"
	dna = 20002_adran_aubergentis
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorenti
	father = 20000
	mother = 20004

	diplomacy = 4
	martial = 4
	stewardship = 4
	intrigue = 4
	learning = 4
	prowess = 4

	trait = physique_good_1
	
	1020.12.4 = {
		birth = yes
	}
}

20003 = {  #Thayen's (dead) wife
	name = "Elwen"
	dna = 20003_aria_aubergentis
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorenti
	female = yes
	
	985.1.7 = {
		birth = yes
	}

	1005.5.20 = { #Died giving birth to Calas
		death = {
			death_reason = death_childbirth
		}
	}
}

20004 = {  #Thayen's (gamestart) wife
	name = "Susanne"
	dna = 20004_isobel_aubergentis
	dynasty = dynasty_aubergentis
	religion = house_of_minara
	culture = lorenti
	female = yes
	
	trait = temperate
	trait = impatient
	trait = ambitious

	993.1.7 = {
		birth = yes
	}
}

20005 = {  #Cala's elven guardian while Thayen was away at war
	name = "Ivran"
	dna = 20005_ivran
	religion = elven_forebears
	culture = moon_elvish
	
	trait = temperate
	trait = trusting
	trait = lazy

	917.1.7 = {
		birth = yes
		effect = {
			set_to_lowborn = yes
		}
	}

	1012.5.10 = {
		employer = 20000
	}
	
	1019.8.15 = {
		effect = {
			reverse_add_opinion = {
				modifier = ruined_my_heir_opinion
				target = character:20000
			}

			reverse_add_opinion = {
				modifier = mentored_me_opinion
				target = character:20001
			}
		}
	}
}

20006 = {
	name = "Ricain"
	dynasty = dynasty_casnaview
	religion = court_of_adean
	culture = lorenti
	
	965.2.8 = {
		birth = yes
	}

	985.10.10 = {
		add_spouse = 20008
	}
}

20007 = {
	name = "Vernell"
	dynasty = dynasty_casnaview
	religion = court_of_adean
	culture = lorenti

	father = 20006
	mother = 20008
	
	994.4.6 = {
		birth = yes
	}
}

20008 = {
	name = "Sofie"
	#
	religion = court_of_adean
	culture = lorenti
	female = yes
	
	965.1.17 = {
		birth = yes
	}

	985.10.10 = {
		add_spouse = 20006
	}
}


