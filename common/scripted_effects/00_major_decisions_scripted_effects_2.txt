﻿#Anbennar - we've just emptied these effects 

defenders_of_highgod_decision_hall_of_heroes_effect = { }

defenders_of_highgod_decision_effects = { }

strengthen_bloodline_decision_effects = {
	dynasty = {
		add_dynasty_modifier = {
			modifier = strong_blood
		}
	}
}

dynasty_of_many_crowns_effects = {
	dynasty = {
		add_dynasty_modifier = {
			modifier = dynasty_of_many_crowns
		}
		add_dynasty_prestige = monumental_dynasty_prestige_gain
	}
}

appoint_a_righteous_caliph_scripted_effect = { }

mozarabic_bind_the_faith_to_rome_decision_fundamentalist_path_scripted_effect = { }

mozarabic_bind_the_faith_to_rome_decision_righteous_path_scripted_effect = { }

mozarabic_bind_the_faith_to_rome_decision_pluralist_path_scripted_effect = { }

mozarabic_break_with_rome_decision_hof_and_ecumenism_processing_scripted_effect = { }

mozarabic_break_with_rome_decision_fundamentalist_path_scripted_effect = { }

mozarabic_break_with_rome_decision_righteous_path_scripted_effect = { }

mozarabic_break_with_rome_decision_pluralist_path_scripted_effect = { }
