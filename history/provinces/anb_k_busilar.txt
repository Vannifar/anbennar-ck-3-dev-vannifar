#k_busilar
##d_lioncost
###c_busilar
367 = {		#Busilar

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1657 = {

    # Misc
    holding = none

    # History

}
1658 = {

    # Misc
    holding = church_holding

    # History

}
1659 = {

    # Misc
    holding = city_holding

    # History

}

###c_lioncost
368 = {		#Lioncost

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1671 = {

    # Misc
    holding = city_holding

    # History

}
1672 = {

    # Misc
    holding = none

    # History

}
1673 = {

    # Misc
    holding = church_holding

    # History

}
1674 = {

    # Misc
    holding = none

    # History

}

###c_carverhouse
372 = {		#Carverhouse

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1660 = {

    # Misc
    holding = none

    # History

}
1661 = {

    # Misc
    holding = city_holding

    # History

}

##d_busilari_straits
###c_lions_pass
39 = {		#Lion's Pass

    # Misc
    culture = businori
    religion = cult_of_the_dame

    # History
}
1662 = {

    # Misc
    holding = church_holding

    # History

}
1663 = {

    # Misc
    holding = none

    # History

}
1664 = {

    # Misc
    holding = city_holding

    # History

}

###c_ohitkes
1665 = {

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1669 = {

    # Misc
    holding = city_holding

    # History

}
1670 = {

    # Misc
    holding = none

    # History

}

###c_derancestir
373 = {		#Derancestir

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1650 = {

    # Misc
    holding = church_holding

    # History

}
1651 = {

    # Misc
    holding = none

    # History

}
1653 = {

    # Misc
    holding = none

    # History

}

##d_lorin
###c_lorinhap
1638 = {		#Lorinhap

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
378 = {

    # Misc
    holding = city_holding

    # History

}
1636 = {

    # Misc
    holding = none

    # History

}
1637 = {

    # Misc
    holding = none

    # History

}

###c_lorincrag
374 = {		#Lorincrag

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1633 = {

    # Misc
    holding = none

    # History

}
1634 = {

    # Misc
    holding = none

    # History

}
1635 = {

    # Misc
    holding = church_holding

    # History

}

##d_canno
###c_cannohap
370 = {		#Cannohap

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1654 = {

    # Misc
    holding = none

    # History

}
1655 = {

    # Misc
    holding = none

    # History

}
1656 = {

    # Misc
    holding = church_holding

    # History

}

###c_khenahap
1641 = {	#Khenahap

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = khenahap_mines_01
	}
}
1640 = {

    # Misc
    holding = none

    # History

}
1642 = {

    # Misc
    holding = city_holding

    # History

}

###c_taulos
1643 = {	#Taulos

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1646 = {

    # Misc
    holding = city_holding

    # History

}
1647 = {

    # Misc
    holding = none

    # History

}

##d_stonehold
###c_stonehold
363 = {

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1687 = {

    # Misc
    holding = none

    # History

}
1688 = {

    # Misc
    holding = city_holding

    # History

}

###c_enkets_pass
362 = {		#Enket's Pass

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1689 = {

    # Misc
    holding = city_holding

    # History

}
1690 = {

    # Misc
    holding = none

    # History

}
1691 = {

    # Misc
    holding = none

    # History

}

##d_hortosare
###c_hortosare
1681 = {	#Hortosare

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1682 = {

    # Misc
    holding = none

    # History

}
1683 = {

    # Misc
	holding = none

    # History
}
371 = {

    # Misc
	holding = church_holding

    # History
}

###c_elsine
1685 = {	#Elsine

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1686 = {

    # Misc
    holding = city_holding

    # History

}
1684 = {

    # Misc
    holding = none

    # History

}

##d_lorbet
###c_port_jaher
365 = {		#Port Jaher

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1675 = {

    # Misc
    holding = city_holding

    # History

}
1676 = {

    # Misc
    holding = church_holding

    # History

}
1677 = {

    # Misc
    holding = none

    # History

}

###c_lorbet
366 = {		#Lorbet

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1678 = {

    # Misc
    holding = church_holding

    # History

}
1680 = {

    # Misc
    holding = none

    # History

}

##d_hapaine
###c_hapaine
408 = {		#Hapaine

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1697 = {

    # Misc
    holding = none

    # History

}
1698 = {

    # Misc
    holding = city_holding

    # History

}
1699 = {

    # Misc
    holding = church_holding

    # History

}

###c_cedor
1692 = {	#Cedor

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1693 = {

    # Misc
    holding = city_holding

    # History

}

###c_berkhen
1695 = {	#Berkhen

    # Misc
    culture = businori
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1694 = {

    # Misc
    holding = none

    # History

}
1696 = {

    # Misc
    holding = city_holding

    # History

}
