﻿#Select correct elvenized culture to convert to
convert_to_elvenized_culture_effect = {
	#Alenic
	if = {
		limit = {
			culture = culture:gawedi
		}
		culture:nuralenic = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:nuralenic
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:nuralenic
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:nuralenic
		}
	}
	else_if = {
		limit = {
			culture = culture:wexonard
		}
		culture:thilosian = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:thilosian
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:thilosian
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:thilosian
		}
	}
	#Bulwari
	#Businori
	else_if = {
		limit = {
			culture = culture:businori
		}
		culture:busilari = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:busilari
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:busilari
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:busilari
		}
	}
	else_if = {
		limit = {
			culture = culture:milcori
		}
		culture:arannese = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:arannese
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:arannese
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:arannese
		}
	}
	#Damesheader
	else_if = {
		limit = {
			OR = {
				culture = culture:old_damerian
				culture = culture:lenco_damerian
			}
		}
		culture:damerian = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:damerian
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:damerian
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:damerian
		}
	}
	else_if = {
		limit = {
			culture = culture:vernid
		}
		culture:vernman = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:vernman
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:vernid
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:vernman
		}
	}
	else_if = {
		limit = {
			culture = culture:old_esmari
		}
		culture:esmari = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:esmari
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:old_esmari
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:esmari
		}
	}
	#Divenori
	#Dostanorian
	#Escanni
	else_if = {
		limit = {
			culture = culture:adeanic
		}
		culture:adenner = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:adenner
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:adeanic
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:adenner
		}
	}
	else_if = {
		limit = {
			culture = culture:castanorian
			OR = {
				top_liege.dynasty = dynasty:dynasty_silurion
				dynasty = dynasty:dynasty_silurion
				has_title = title:c_ar_urion
				any_vassal_or_below = {
					has_title = title:c_ar_urion
				}
				top_liege = {
					OR = {
						has_title = title:c_ar_urion
						any_vassal_or_below = {
							has_title = title:c_ar_urion
						}
					}
				}
			}
		}
		culture:urionmari = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:urionmari
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:urionmari
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:urionmari
		}
	}
	#Only for Castellyr formation?
	# else_if = {
		# limit = {
			# culture = culture:castanorian
		# }
		# culture:castellyrian = {
			# if = {
				# limit = {
					# NOT = {
						# any_province = {
							# culture = culture:castellyrian
						# }
					# }
				# }
				# get_all_innovations_from = root.culture
			# }
		# }
		# convert_family_culture_and_notify_vassals_effect = {
			# CONVERTER = root
			# OLD_CULTURE = root.culture
			# NEW_CULTURE = culture:castellyrian
		# }
		# capital_county = {
			# if = {
				# limit = { culture = root.culture }
				# set_county_culture = culture:castellyrian
			# }
		# }
	# }
	else_if = {
		limit = {
			culture = culture:marcher
		}
		culture:calindalic = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:calindalic
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:calindalic
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:calindalic
		}
	}
	#Gerudian
	#Lencori
	else_if = {
		limit = {
			culture = culture:lorenti
		}
		culture:lorentish = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:lorentish
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:lorentish
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:lorentish
		}
	}
	else_if = {
		limit = {
			culture = culture:sormanni
		}
		culture:sorncosti = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:sorncosti
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:sorncosti
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:sorncosti
		}
	}
	#Eborthili
	else_if = {
		limit = {
			culture = culture:tefori
		}
		culture:tefori = {
			if = {
				limit = {
					NOT = {
						any_province = {
							culture = culture:eborthili
						}
					}
				}
				get_all_innovations_from = root.culture
			}
		}
		capital_county = {
			if = {
				limit = { culture = root.culture }
				set_county_culture = culture:eborthili
			}
		}
		convert_family_culture_and_notify_vassals_effect = {
			CONVERTER = root
			OLD_CULTURE = root.culture
			NEW_CULTURE = culture:eborthili
		}
	}
}