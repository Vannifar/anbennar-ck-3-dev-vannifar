﻿name_list_halfling = {

	dynasty_names = {
		"dynn_Abdena"
		"dynn_Allena"
		"dynn_Andringa"
		"dynn_Bauwenga"
		"dynn_Beninga"
		"dynn_Boltinge"
		"dynn_Botta"
		"dynn_Buning"
		"dynn_Butzel"
		"dynn_Cammingha"
		"dynn_Cirksema"
		"dynn_Clant"
		"dynn_Coenders"
		"dynn_Cordinghe"
		"dynn_Dives"
		"dynn_Ewsum"
		"dynn_Folkerdinga"
		"dynn_Gaykema"
		"dynn_Ghetzerka"
		"dynn_Gockinga"
		"dynn_Goltraven"
		"dynn_Haijkama"
		"dynn_Harting"
		"dynn_Heddama"
		"dynn_Hedegu"
		"dynn_Hedinge"
		"dynn_Hornada"
		"dynn_Hornekingh"
		"dynn_Houwerda"
		"dynn_Huginge"
		"dynn_Huninga"
		"dynn_Huwenga"
		"dynn_Idzarda"
		"dynn_Ildsisma"
		"dynn_Jarges"
		"dynn_Kalemere"
		"dynn_Kater"
		"dynn_Korenporta"
		"dynn_Kote"
		"dynn_Lewe"
		"dynn_Manninga"
		"dynn_Martena"
		"dynn_Meckema"
		"dynn_Mernseta"
		"dynn_Niding"
		"dynn_Oenema"
		"dynn_Omken"
		"dynn_Ommeloop"
		"dynn_Onsta"
		"dynn_Papinge"
		"dynn_Poder"
		"dynn_Pol"
		"dynn_Popinghe"
		"dynn_Potens"
		"dynn_Reding"
		"dynn_Remian"
		"dynn_Rengers"
		"dynn_Rickwardsma"
		"dynn_Ripperda"
		"dynn_Rodmersma"
		"dynn_Schulte"
		"dynn_Serda"
		"dynn_Sickinge"
		"dynn_Spegel"
		"dynn_Stickel"
		"dynn_Suanckerus"
		"dynn_Swaneke"
		"dynn_Tayingha"
		"dynn_Tiddinga"
		"dynn_Tjarksena"
		"dynn_Ukena"
		"dynn_Umbelap"
		"dynn_Wiemken"
		{ "dynnp_de" "dynn_Aggere" }
		{ "dynnp_de" "dynn_Helpman" }
		{ "dynnp_de" "dynn_Mepsche" }
		{ "dynnp_van" "dynn_Hallum" }
	}
	
	male_names = {
		Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas CastE_n CastI_n Daran 
		Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Nara Olor OtO_ ReA_n RiannO_n Ricain RI_on Robin Rogier Sandur Taelar 
		Thal Thiren TomA_s Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Lucian Luciana Dominic Vernell Arman Cecill Tristan Alvar Adelar 
		Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Corin Henry Henric Edd Edward James Willam Ricard Philip Fred Frederic Artur Charles Edgar 
		Thom Thomas Tomban Tombard TomA_s Edmund Georg Jon Jacob Humphrey Robert Stephen Stefen Stefan Adam Albert Alfred Andrew Anton Antony Archibald 
		Ben Benjamin Christopher Davan Devan Delian Ernest Ernst Francis Geoffrey Horatio Hugh Isaac Jeremy Jonathan Joseph 
		Lomban Lombar Lombas Lomben Lomen Lommy Loman Lomas Lomar Samuel Sam Roy Humfrey Humbar Humban Roger Petran
		Erel Crovan Crovis Carlan Gracos Allan Allen Maycald Mikel Nicolas Filip Athan Creston Cristof Mathos Alaric Adal Alberic Alberan Alber Alb Balbo Belbas Frodan 
		Bobban Briff Briffo Briffan Caradoc Cradoc Merian Merrian Cerdic Cerd Conwell Conrad Contan Dudo Erling Folcard Folcas Grif Hal Halfred Hamfast Andwise Anders 
		Garret Garet Garman Percival Pervince Tobias Rufus Rudolf Roderic Rewan Borian Lothan Lothane Lain OtO_ Humbert Kolin Ricain
	}
	female_names = {
		Adra Alara Amarien Aria Calassa Aucanna CastE_nnia CastI_na Cela Celadora Clarimonde EilI_s EilI_sabet Erela Erella Galina Galinda Laurenne Lianne 
		MadalE_in Maria Marianna Mariana Marianne Marien Marina RE_anna Thalia Varina Varinna Alina Arabella Margery Cecille Kerstin Alisanne Isobel Isabel 
		Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance ElE_anore Valence EmilI_e GisI_le Athana Corina Cora Lisolette
		Jane Angelica Arabel Arabella Adela Adelaide Alice Amice Anne Anna Aveline Avelina Beatrice Blanche Belinda Bella Katerine Katrine Cecilia 
		Constance Edith Ela Eleanor Elizabeth Emma Hawise Isabel Isabella Elisabeth Joan Joanna 
		Juliana Margaret Mary Maria Maud Philippa Sybilla Sybille Dora Miranda Mirabella Primrose Yolanda
		Ruby Rose Belladonna Matilda Myrtle Poppy Selina Pearl Nora Athana
		Marianne Erela Erella Laurenne
	}

	dynasty_of_location_prefix = "dynnp_of"	

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 30
	mat_grf_name_chance = 10
	father_name_chance = 5
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 20
	mat_grm_name_chance = 40
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}
