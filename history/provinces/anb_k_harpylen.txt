#k_harpylen
##d_harpylen
###c_harpylen
660 = {		#Harpylen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_atmontna
663 = {		#Atmonta

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_fernnduim
661 = {		#Fernnduim

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

##d_alyzksaan
###c_alyzksaan
659 = {		#Alyzksaan

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_sheezeul
658 = {		#Sheezeul

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_tenoahlen
662 = {		#Tenoahlen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_zaggal
657 = {		#Zaggal

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_koozk
651 = {		#Koozk

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

##d_crelyore
###c_frenyantlen
666 = {		#Frenyantlen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_matzaul
667 = {		#Matzaul

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}