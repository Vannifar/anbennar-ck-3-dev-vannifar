#k_viswall
##d_viswall
###c_viswall
1213 = {	#Rainbow Hall

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
    holding = castle_holding

    # History

}
63 = {		#South Viswall

    # Misc
	holding = city_holding

    # History
}
66 = {		#North Viswall

    # Misc
	holding = city_holding

    # History
}

##d_barrowshire
###c_barrowshire
164 = {		#Barrowshire

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = city_holding

    # History
}

###c_coppertown
160 = {		#Coppertown

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1214 = {

    # Misc
    holding = castle_holding

    # History

}
1215 = {

    # Misc
    holding = church_holding

    # History

}
1216 = {

    # Misc
    holding = city_holding

    # History

}

###c_hillwater
161 = {		#Hillwater

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1212 = {

    # Misc
    holding = city_holding

    # History

}

###c_aelcandar
162 = {	

    # Misc
	holding = church_holding

    # History
}
1227 = {    #Aelcandar

    # Misc
    culture = lorentish
    religion = court_of_adean
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = calasandur_castle_aelcandar_01
		special_building = calasandur_castle_aelcandar_01
	}

}
1228 = {

    # Misc
    holding = city_holding

    # History

}

1208 = {

    # Misc
    holding = none

    # History

}

##d_greymill
###c_greymill
12 = {		#Greymill

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1202 = {

    # Misc
    holding = none

    # History

}
1203 = {

    # Misc
    holding = city_holding

    # History

}

###c_norley
149 = {		#Norley

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1205 = {

    # Misc
    holding = none

    # History

}
1206 = {

    # Misc
    holding = none

    # History

}
1207 = {

    # Misc
    holding = city_holding

    # History

}

###c_old_course
76 = {		#Coursefort

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1204 = {

    # Misc
    holding = none

    # History

}

##d_thomsbridge
###c_thomsbridge
159 = {		#Thomsbridge

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1217 = {

    # Misc
    holding = city_holding

    # History

}
1218 = {

    # Misc
    holding = none

    # History

}
1219 = {

    # Misc
    holding = church_holding

    # History

}
1220 = {

    # Misc
    holding = none

    # History

}
1221 = {

    # Misc
    holding = none

    # History

}

###c_merryfield
166 = {		#Merryfield

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1225 = {

    # Misc
    holding = city_holding

    # History

}
1226 = {

    # Misc
    holding = church_holding

    # History

}
1229 = {

    # Misc
    holding = none

    # History

}

###c_tipney
135 = {		#Tipney

    # Misc
    culture = hillfoot_halfling
    religion = small_temple
	holding = city_holding

    # History
}
1222 = {

    # Misc
    holding = none

    # History

}
1223 = {

    # Misc
    holding = none

    # History

}
1224 = {

    # Misc
    holding = city_holding

    # History

}
