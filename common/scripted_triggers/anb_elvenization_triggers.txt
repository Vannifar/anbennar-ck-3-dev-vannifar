﻿#Cultures that can be elvenized, used within culture scope
culture_can_be_elvenized_trigger = {
	OR = {
		#Alenic
		this = culture:gawedi #Nuralenic
		this = culture:wexonard #Thilosian
		#Bulwari
		#Businori
		this = culture:businori #Busilari
		this = culture:milcori #Arannese
		this = culture:tefori #Tefori
		#Damesheader
		this = culture:old_damerian #Damerian
		this = culture:lenco_damerian #Damerian
		this = culture:vernid #Vernman
		this = culture:old_esmari #Esmari
		#Divenori
		#Dostanorian
		#Escanni
		this = culture:adeanic #Adenner
		AND = {
			this = culture:castanorian #Castellyrian, Urionmari
			OR = {
				root.top_liege.dynasty = dynasty:dynasty_silurion
				root.dynasty = dynasty:dynasty_silurion
				root = {
					OR = {
						has_title = title:c_ar_urion
						any_vassal_or_below = {
							has_title = title:c_ar_urion
						}
					}
				}
				root.top_liege = {
					OR = {
						has_title = title:c_ar_urion
						any_vassal_or_below = {
							has_title = title:c_ar_urion
						}
					}
				}
			}
		}
		this = culture:marcher #Calindalic
		#Gerudian
		#Lencori
		this = culture:lorenti #Lorentish
		this = culture:sormanni #Sorncosti
	}
}

#Cultures that are elvenized, used within culture scope
culture_is_elvenized_trigger = {
	OR = {
		#Alenic
		this = culture:nuralenic #Gawedi, other north Alenic?
		this = culture:thilosian #Wexonard
		#Bulwari
		#Businori
		this = culture:busilari #Businori
		this = culture:eborthili #Eborthili
		#Damesheader
		this = culture:damerian #Old Damerian, Lenco-Damerian, Carnetori?
		this = culture:vernman #Vernid
		this = culture:esmari #Old Esmari, Ryalani?
		this = culture:arannese #Milcori
		this = culture:arbarani #Crownsman?, just Arbaran kingdom?
		#Divenori
		#Dostanorian
		#Escanni
		this = culture:adenner #adeanic
		this = culture:castellyrian #Castanorian
		this = culture:urionmari #Castanorian
		this = culture:calindalic #Marcher
		#Gerudian
		#Lencori
		this = culture:lorentish #Lorenti
		this = culture:sorncosti #Sormanni
	}
}

#Requirements for elvenization innovation
can_adopt_elvenization_innovation = {
	OR = {
		# is_elvish_culture = yes
		has_cultural_pillar = heritage_elven
		culture_is_elvenized_trigger = yes
		AND = {
			exists = culture_head
			culture_head = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					AND = {
						exists = primary_spouse
						primary_spouse = {
							OR = {
								has_trait = race_elf
								has_trait = race_half_elf
							}
						}
					}
					any_councillor = {
						is_powerful_vassal = yes
						OR = {
							has_trait = race_elf
							has_trait = race_half_elf
						}
					}
					capital_province.culture = {
						has_innovation = innovation_elvenization
					}
				}
			}
		}
	}
}