﻿namespace = anb_minor_decisions

#pearlsedger_cut_hair_decision
# minor_decisions.0001 - cut the hair

#Shaving your head for pearlsedger_cut_hair_decision
anb_minor_decisions.0001 = {
	type = character_event
	title = anb_minor_decisions.0001.t
	desc = anb_minor_decisions.0001.desc
	theme = martial
	left_portrait = {
		character = root
		animation = personality_honorable
	}

	#The ancestors!
	option = {
		name = anb_minor_decisions.0001.a

		add_prestige = 10

		ai_chance = {
			#Only option.
			base = 100
		}
	}
}

# This makes the Pearlsedger landless warriors/married men bald
anb_minor_decisions.0002 = {
	type = character_event
	hidden = yes

	trigger = {
		has_culture = culture:pearlsedger
		is_ruler = no
		NOT = {
			has_character_flag = anb_minor_decisions_0001_pearlsedger_cut_hair_decision_text
		}
		OR = {
			trigger_if = {
				limit = {
					is_female = no
				}
				is_married = yes
			}
			trigger_if = {
				limit = {
					is_female = yes
				}
				martial >= 30
			}
			trigger_else = {
				martial >= 20
			}
			trigger_if = {
				limit = {
					is_female = yes
				}
				prowess >= 30
			}
			trigger_else = {
				prowess >= 20
			}
		}
	}
	#The ancestors!
	immediate = {
		add_character_flag = anb_minor_decisions_0001_pearlsedger_cut_hair_decision_text
		add_prestige = 10
	}
}

anb_minor_decisions.0003 = {
	type = character_event
	title = anb_minor_decisions.0003.title
	desc = anb_minor_decisions.0003.desc

	theme = crown
	right_portrait = {
		character = root
		animation = happiness
	}
	
	immediate = {
		random_character_artifact = {
			limit = { has_variable = is_carillon_of_the_lunetein }
			reforge_artifact = {
				max_durability = 100
				decaying = no
			}
			remove_variable = is_carillon_of_the_lunetein
			set_variable = is_repaired_carillon_of_the_lunetein
		}
	}
	
	option = {
		name = anb_minor_decisions.0003.a
		add_prestige = 1000
	}
}
