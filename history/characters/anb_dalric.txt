﻿300000 = {
	name = "Gunnarr"
	dna = 300000_gunnarr
	dynasty = dynasty_bjarnsson
	religion = skaldhyrric_faith
	culture = dalric
	father = 300005
	mother = 300007

	diplomacy = 4
	martial = 10
	stewardship = 4
	intrigue = 6
	learning = 4
	prowess = 13

	trait = wrathful
	trait = arrogant
	trait = temperate
	trait = education_martial_3
	trait = physique_good_1
	trait = scarred
	trait = irritable
	trait = aggressive_attacker
	
	1003.3.11 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
			change_current_weight = {value = current_weight multiply = -1}
			change_target_weight = {value = target_weight multiply = -1}
		}
	}
	1017.1.1 = {
		add_spouse = 300001
	}
	1020.10.31 = {
		set_artifact_rarity_illustrious = yes
		create_artifact = {
			name = ebonsteel_sword_of_bjarnrik_name
			description = ebonsteel_sword_of_bjarnrik_description
			type = sword
			template = ebonsteel_sword_of_bjarnrik_template
			visuals = sword
			wealth = scope:wealth
			quality = scope:quality
			history = {
				type = inherited
				date = 1020.10.31
			}
			modifier = ebonsteel_sword_of_bjarnrik_modifier
			save_scope_as = newly_created_artifact
			decaying = no
		}
		scope:newly_created_artifact = {
			set_variable = { name = historical_unique_artifact value = yes }
		}
	}
}

300001 = {
	name = "Haelga"
	dna = 300001_haelga
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	
	trait = sadistic
	trait = zealous
	trait = stubborn
	trait = shieldmaiden
	trait = education_learning_1
	
	diplomacy = 2
	martial = 6
	stewardship = 6
	intrigue = 8
	learning = 5
	prowess = 12
	
	1002.1.1 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
			change_current_weight = {value = current_weight multiply = -1}
			change_target_weight = {value = target_weight multiply = -1}
		}
	}
}

300002 = {
	name = "Skali"
	dna = 300002_skali
	dynasty = dynasty_sidaett
	religion = skaldhyrric_faith
	culture = dalric
	
	trait = just
	trait = content
	trait = diligent
	trait = education_stewardship_3
	trait = administrator
	trait = cautious_leader
	
	diplomacy = 10
	martial = 8
	stewardship = 8
	intrigue = 0
	learning = 4
	prowess = 6
	
	965.1.1 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	980.1.1 = {
		add_spouse = 300004
	}
}

300003 = {
	name = "Einar"
	dna = 300003_einar
	religion = skaldhyrric_faith
	culture = dalric
	
	trait = brave
	trait = ambitious
	trait = greedy
	trait = giant
	trait = education_martial_3
	trait = physique_good_1
	trait = viking
	
	diplomacy = 3
	martial = 12
	stewardship = 8
	intrigue = 10
	learning = 2
	prowess = 13
	
	990.1.1 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
			change_current_weight = {value = current_weight multiply = -1}
			change_target_weight = {value = target_weight multiply = -1}
		}
	}
}

300004 = {
	name = "Sigrid"
	#dna = 300004_sigrid
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	
	967 = {
		birth = yes
	}
}

300005 = {
	name = "Magnus"
	dynasty = dynasty_bjarnsson
	#dna = 300005_magnus
	religion = skaldhyrric_faith
	culture = dalric
	
	trait = craven
	trait = gregarious
	trait = lazy
	trait = education_diplomacy_1
	
	983 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	990 = {
		add_spouse = 30007
	}
	1020.10.31 = {
		death = {
			death_reason = death_murder
		}
	}
}

300006 = {
	name = "Totil"
	dynasty = dynasty_bjarnsson
	religion = skaldhyrric_faith
	culture = dalric
	father = 300005
	mother = 300007
	
	trait = charming
	
	1013.12.7 = {
		birth = yes
	}
	1020.10.31 = {
		employer = 300002
	}
}

300007 = {
	name = "Katrin"
	dynasty = dynasty_sidaett
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	father = 300002
	
	986 = {
		birth = yes
	}
}

300008 = {
	name = "Gjertrud"
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	
	997 = {
		birth = yes
	}
}

300009 = {
	name = "Oskar"
	religion = skaldhyrric_faith
	culture = dalric
	
	979 = {
		birth = yes
	}
}

300010 = {
	name = "Aela"
	religion = skaldhyrric_faith
	culture = dalric
	female = yes
	
	962 = {
		birth = yes
	}
}

300011 = {
	name = "Bolli"
	religion = skaldhyrric_faith
	culture = dalric
	
	1001 = {
		birth = yes
	}
}

300019 = {
	name = "Sigurd"
	religion = skaldhyrric_faith
	culture = dalric
	dynasty = dynasty_bjarnsson
	father = 300000
	
	1020 = {
		birth = yes
	}
}

300020 = {
	name = "Oskar"
	religion = skaldhyrric_faith
	culture = dalric
	dynasty = dynasty_sidaett
	father = 300002
	mother = 300004
	
	diplomacy = 6
	martial = 10
	stewardship = 6
	intrigue = 4
	learning = 5
	prowess = 9
	trait = education_diplomacy_2
	
	trait = brave
	trait = lustful
	trait = just
	trait = scarred
	trait = lifestyle_blademaster
	
	990 = {
		birth = yes
	}
}