﻿skaldhyrric_dirge = {
	icon = magic_positive
	naval_movement_speed_mult = 0.2
	army_maintenance_mult = -0.1
}

skaldhyrric_gjalund = {
	icon = magic_positive
	prowess_per_prestige_level = 2
	health = minor_health_bonus
}

skaldhyrric_beralic = {
	icon = magic_positive
	vassal_levy_contribution_mult = 0.1
	vassal_tax_contribution_mult = 0.1
}

skaldhyrric_voyage = {
	icon = magic_positive
	raid_speed = 0.2
	max_loot_mult = 0.2
}

skaldhyrric_golden_forest = {
	icon = magic_positive
	domain_tax_mult = 0.05
	build_gold_cost = -0.1
	build_prestige_cost = -0.1
}

skaldhyrric_master_boatbuilders = {
	icon = magic_positive
	hostile_county_attrition = -0.2
	embarkation_cost_mult = -0.25
}

skaldhyrric_dragon_and_skald = {
	icon = magic_positive
	development_growth_factor = 0.1
	learning_per_prestige_level = 2
}

skaldhyrric_old_winter_lullaby = {
	icon = magic_positive
	general_opinion = 10
	monthly_county_control_change_factor = 0.15
	county_opinion_add = 10
}