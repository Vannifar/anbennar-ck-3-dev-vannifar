anb_game_start = {
	effect = {
		#ANBENNAR
		every_living_character = {
			limit = {
				is_elvish_culture = yes
			}
			if = {
				limit = {
					NOT = { has_racial_trait = yes }
				}
				add_trait = race_elf
			}
			#set_immortal_age = 20
		}
		
		character:20 = {
			set_immortal_age = 20
		}
		
		character:21 = {
			set_immortal_age = 20
		}

		character:sun_elvish0004 = {
			set_immortal_age = 20
		}

		character:519 = {
			set_immortal_age = 25
		}

		character:520 = {
			set_immortal_age = 25
		}

		character:545 = {
			set_immortal_age = 25
		}

		character:546 = {
			set_immortal_age = 25
		}
		
		character:547 = {
			set_immortal_age = 25
		}

		character:701 = {
			set_immortal_age = 25
		}

		character:702 = {
			set_immortal_age = 25
		}

		every_living_character = {
			limit = {
				is_human_culture = yes
			}
			if = {
				limit = {
					NOT = { has_racial_trait = yes }
				}
				add_trait = race_human
			}
		}
		
		every_living_character = {
			limit = {
				is_dwarvish_culture = yes
			}
			if = {
				limit = {
					NOT = { has_racial_trait = yes }
				}
				add_trait = race_dwarf
			}
			set_immortal_age = 40
		}
		
		every_living_character = {
			limit = {
				is_halfling_culture = yes
			}
			if = {
				limit = {
					NOT = { has_racial_trait = yes }
				}
				add_trait = race_halfling
			}
		}
		
		every_living_character = {
			limit = {
				is_gnomish_culture = yes
			}
			if = {
				limit = {
					NOT = { has_racial_trait = yes }
				}
				add_trait = race_gnome
			}
			set_immortal_age = 35
		}

		every_living_character = {
			limit = {
				has_culture = culture:pearlsedger
			}
			if = {
				limit = {
					NOT = {
						has_character_flag = anb_minor_decisions_0001_pearlsedger_cut_hair_decision_text
					}
					OR = {
						trigger_if = {
							limit = {
								is_female = no
							}
							is_married = yes
						}
						trigger_if = {
							limit = {
								is_female = yes
							}
							martial >= 30
						}
						trigger_else = {
							martial >= 20
						}
						trigger_if = {
							limit = {
								is_female = yes
							}
							prowess >= 30
						}
						trigger_else = {
							prowess >= 20
						}
					}
				}
				add_character_flag = {
					flag = anb_minor_decisions_0001_pearlsedger_cut_hair_decision_text
					days = -1
				}
				add_prestige = 10
			}
		}

		title:e_bulwar = { set_coa = e_phoenix_empire }
		
		###Anbennar - Cult of nerat has been reformed into court of nerat.
		faith:cult_of_nerat = { 
			set_variable = { name = has_been_reformed }
		}
		
		mark_faiths_to_found = yes
	}
}

anb_on_game_start_after_lobby = {
	effect = {
		#Anbennar - Setup Racial Purity
		if = {
			limit = { has_game_rule = legitimacy_and_supremacy_on }
			#Add racial attitude to characters
			every_living_character = {
				limit = { NOT = { has_variable = no_purist_trait } } # Don't give it to characters marked as not racist
				roll_racial_purity_trait_for_character = yes
			}
		}
		
		character:48 = {
			if = {
				limit = {
					is_alive = yes
					is_landed = yes
				}
				trigger_event = {
					id = anb_holy_orders.1 # Saltmarcher Creation
					days = -1
				}
			}
		}
	}
}