﻿give_corset_burial_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_personal_religious.dds"

	desc = give_corset_burial_decision_desc
	selection_tooltip = give_corset_burial_decision_tooltip

	is_shown = {
		faith = {
			has_doctrine_parameter = corset_burials_active
		}
		has_variable = ancestor_to_bury
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
	}

	effect = {
		show_as_tooltip = {
			add_piety = major_piety_value
			if = {
				limit = {
					any_vassal = {
						faith = {
							has_doctrine_parameter = corset_burials_active
						}
					}
				}
				every_vassal = {
					limit = {
						faith = {
							has_doctrine_parameter = corset_burials_active
						}
					}
					custom = give_corset_burial_vassals
					add_opinion = {
						modifier = pleased_opinion
						target = root
						opinion = 20
					}
				}
			}
		}
		trigger_event = anb_religious_decision.0001
	}

	ai_check_interval = 36
	
	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

recite_skaldic_tale_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_personal_religious.dds"
	desc = recite_skaldic_tale_desc
	selection_tooltip = recite_skaldic_tale_tooltip
	
	is_shown = {
		faith = {
			has_doctrine_parameter = skaldic_tales_active
		}
		has_skaldic_tale_active = no
	}
	
	is_valid = {
	}
	is_valid_showing_failures_only = {
	}
	
	cost = {
		piety = 100
	}
	
	effect = {
		show_as_tooltip = {
			if = {
				limit = {
					any_vassal = {
						faith = {
							has_doctrine_parameter = skaldic_tale_active
						}
					}
				}
				every_vassal = {
					limit = {
						faith = {
							has_doctrine_parameter = skaldic_tales_active
						}
					}
					custom = recite_skaldic_tale_vassals
					add_opinion = {
						modifier = pleased_opinion
						target = root
						opinion = 10
					}
				}
			}
		}
		trigger_event = anb_religious_decision.0501
	}
	ai_check_interval = 36
	
	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}