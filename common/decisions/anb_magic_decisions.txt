﻿### Forbidden Magic ###

reveal_as_practitioner_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_realm.dds"
	major = yes
	desc = reveal_as_practitioner_decision_desc
	selection_tooltip = reveal_as_practitioner_decision_tooltip
	
	is_shown = {
		any_secret = { secret_type = secret_forbidden_magic }
	}
	
	is_valid = {
		custom_description = {
			text = reveal_as_practitioner_decision_requirement
			any_secret = { secret_type = secret_forbidden_magic }
		}
	}
	
	cost = {
		prestige = 250
	}
	
	effect = {
		gain_forbidden_magic_trait_effect = yes
	}
	
	ai_check_interval = 360

	ai_potential = {
		is_landed = yes
	}
	
	ai_will_do = {
		base = 50
		
		ai_value_modifier = {
			NOT = {
				trait_is_shunned_or_criminal_in_faith_trigger = {
					TRAIT = forbidden_magic_practitioner
					GENDER_CHARACTER = root		
				}
			}
			ai_boldness = 1
			ai_rationality = -2
		}
		
		ai_value_modifier = {
			trait_is_shunned_in_faith_trigger = {
				TRAIT = forbidden_magic_practitioner
				GENDER_CHARACTER = root		
			}
			ai_boldness = 1
			ai_rationality = -3
		}
		
		modifier = {
			trait_is_criminal_in_faith_trigger = {
				TRAIT = forbidden_magic_practitioner
				FAITH = root.faith
				GENDER_CHARACTER = root		
			}
			factor = 0
		}	
	}
}

fight_the_domination_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_realm.dds"
	major = yes
	desc = fight_the_domination_decision_desc
	selection_tooltip = fight_the_domination_decision_tooltip
	
	is_shown = {
		exists = from.this.var:controller
		NOT = { owns_story_of_type = anb_story_cycle_break_domination }
	}
	
	is_valid = {
		stress = 0
	}
	
	cooldown = { years = 10 }
	
	cost = {}
	
	effect = {
		create_story = {type = anb_story_cycle_break_domination}
	}
	
	ai_check_interval = 30

	ai_potential = {
		always = yes
	}
	
	ai_will_do = {
		base = 100
	}
}