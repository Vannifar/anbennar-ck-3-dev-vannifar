﻿default=jungle

#E_Anbennar

#K_Damescrown

#Damescrown
233=farmlands
234=farmlands
249=farmlands
915=plains
2035=farmlands
2036=farmlands
2037=plains
2038=forest
2039=plains
2040=plains
2041=plains
2042=forest
2043=plains
2044=plains

#Aranmas
254=plains
255=plains
262=plains
908=plains
1886=plains
1887=plains
1888=plains
1890=farmlands
1892=plains
1893=forest
1901=forest
1904=forest

#Crodam
236=plains
250=plains
253=plains
258=plains
334=plains
335=plains
342=plains
357=hills
914=plains
2045=plains
2046=plains
2047=plains
2048=plains
2049=plains
2050=plains
2051=hills
2052=plains
2053=hills
2054=plains
2078=plains
2079=plains
2080=plains
2081=plains
2082=plains
2083=plains
2084=plains
2085=plains
2086=plains
2087=plains

#K_Esmaria

#Ryalanar
264=plains
265=plains
575=forest
917=forest
1848=plains
1849=forest
1850=plains
1851=forest
1852=plains

#Cann Esmar
259=plains
260=plains
266=plains
267=plains
911=plains
1871=plains
1872=plains
1873=plains
1874=plains
1875=farmlands
1876=farmlands
1877=plains
1878=plains
1879=hills
1880=farmlands
1881=mountains
1882=forest
1883=forest
1884=plains

#Bennonhill
263=mountains
268=farmlands
270=mountains
271=plains
909=plains
1905=plains
1906=mountains
1910=plains
1911=mountains

#Konwell
269=forest
275=plains
905=forest
1841=plains
1842=farmlands
1843=forest
1844=forest
1845=forest
1846=plains
1847=forest
1907=farmlands
1909=farmlands
1912=plains
1913=forest
1914=forest

#Songbarges
274=farmlands
279=plains
333=plains
916=plains
1853=farmlands
1854=plains
1855=plains
1856=plains
1857=plains
1858=plains
1859=plains
1860=plains

#Hearthswood
272=forest
273=plains
900=forest
1931=plains
1933=forest
1934=forest
1935=forest
1936=plains

#Estallen
44=farmlands
280=farmlands
297=farmlands
301=hills
576=forest
1861=plains
1862=plains
1863=plains
1864=hills
1865=plains
1866=farmlands
1867=forest
1868=forest
1869=hills
1870=hills

#Leslinpár
302=plains
309=plains
901=farmlands
1915=forest
1916=plains
1917=forest
1918=forest
1925=plains
1930=forest

#Silverforge
308=forest
912=dwarven_hold_surface
1937=forest
1938=mountains
1939=hills

#Asheniande
310=plains
311=plains
314=hills
320=plains
902=plains
1722=mountains
1723=plains
1724=plains
1940=forest
1941=forest
1942=plains
1943=plains
1944=forest
1945=plains
1946=plains
1947=plains

#K_Verne

#Eastneck
25=farmlands
45=plains
50=farmlands
51=plains
1817=plains
1818=forest
1819=plains
1820=plains
1825=forest
1826=plains

#Menibor Looop
284=farmlands
285=farmlands
331=farmlands
569=forest
1822=farmlands
1823=forest
1824=plains
1827=farmlands
1828=plains
1829=plains
1830=plains
1831=plains
1832=plains
1833=plains
1834=plains

#Galeinn
287=farmlands
295=farmlands
906=hills
1835=forest
1836=plains
1837=hills
1838=hills
1839=plains
1840=plains

#Armanhal
283=plains
286=plains
364=plains
1811=plains
1813=plains
1815=plains
1816=plains
1821=plains

#Verne
288=forest
289=hills
292=forest
293=plains
1806=forest
1807=forest
1808=plains
1809=plains
1810=forest

#Wyvernmark
290=plains
291=plains
838=hills
1800=hills
1801=plains
1802=hills
1803=plains
1804=hills
1805=hills

#The Tail
375=hills
376=hills
377=mountains
1790=hills
1791=hills
1792=hills
1796=hills
1797=mountains
1798=hills
1799=mountains

#K_Wex

#Wexhills
303=hills
304=hills
305=hills
306=hills
2126=hills
2127=hills
2128=hills
2129=hills
2130=hills
2131=hills
2132=hills
2133=hills
2134=hills

#Ottocam
307=hills
903=forest
907=plains
1775=hills
1776=forest
1777=hills
2118=forest
2119=forest
2120=hills
2121=hills
2122=hills
2123=plains
2124=hills
2125=plains

#Bisan
294=plains
300=plains
315=farmlands
332=plains
579=farmlands
923=plains
2088=plains
2089=plains
2090=plains
2091=plains
2092=forest
2093=plains
2094=plains
2095=plains
2096=plains
2097=farmlands
2098=plains

#Escander
316=hills
317=hills
913=plains
2106=mountains
2107=plains
2108=hills
2109=hills
2110=hills

#Greater Bardswood
318=forest
319=plains
918=forest
2099=plains
2100=plains
2101=forest
2102=forest
2103=forest
2104=forest
2105=forest

#Sugamber
415=plains
919=plains
2111=hills
2112=hills
2113=plains
2114=plains
2115=hills
2116=plains
2117=plains

#K_Borders

#Gisden
312=plains
321=plains
326=plains
920=plains
2176=forest
2177=plains
2178=plains
2179=plains
2180=forest
2181=plains

#Arannen
313=forest
325=plains
418=forest
904=farmlands
2160=forest
2161=forest
2162=forest
2163=forest
2164=plains
2165=forest
2166=forest
2167=plains
2168=plains
2169=plains
2170=plains

#Brinkwick
322=forest
323=plains
324=plains
421=plains
2171=forest
2172=plains
2173=wetlands
2174=plains
2175=plains

#Tellum
414=hills
417=hills
2154=hills
2155=hills
2156=hills
2157=hills

#Antirhal
416=hills
419=hills
420=hills
2149=hills
2150=forest
2151=forest
2152=hills
2153=hills
2158=hills
2159=hills

#Hawkfields
409=plains
410=plains
411=plains
2135=hills
2136=hills
2137=plains
2138=hills
2139=plains
2140=plains

#Highcliff
412=plains
413=plains
921=plains
922=forest
2141=hills
2142=forest
2143=forest
2144=forest
2145=plains
2146=forest
2147=plains
2148=forest

#K_Dameria

#Damesear
1=forest
2=hills	
3=plains
4=mountains
5=forest	
6=plains
8=farmlands
9=hills
11=plains
168=plains
924=farmlands
925=farmlands
926=plains
927=farmlands
928=plains
929=forest
930=plains
931=plains
932=forest
933=forest
934=plains
935=plains
936=plains
938=plains
1041=forest
1042=hills
1043=hills
1044=mountains
1045=hills
1046=hills
1047=plains
1048=forest

524=hills	#Ainathil

#Exwes
18=plains	
153=plains
154=plains
156=plains
939=plains
940=mountains
1062=plains
1063=plains

#Wesdam
10=plains
13=plains
15=plains
17=forest
20=forest
28=plains
31=forest
937=plains
1012=plains
1059=forest
1060=forest
1061=plains

#Neckcliffe
16=plains
21=forest
23=plains
24=plains
26=farmlands
27=plains
941=forest
942=plains
1049=plains
1050=forest

#Acromton
276=hills
330=hills
1742=hills
1743=hills
1744=hills
1745=forest
1746=plains
1747=plains

#Istralore
37=hills
277=hills
899=hills
1733=forest
1734=forest
1735=forest
1736=hills
1737=hills
1738=hills
1739=hills
1740=hills
1741=hills
1748=forest

#Silverwoods
29=forest
52=farmlands
53=forest
54=forest
55=forest
56=forest
1726=forest
1727=forest
1728=forest
1729=forest
1730=forest
1731=forest
1732=forest

#Plumwall
281=plains
328=plains
910=plains
1749=plains
1750=plains
1751=plains
1752=farmlands
1753=farmlands
1754=farmlands
1755=plains
1756=plains
1757=plains
1773=plains

#Heartlands
278=plains
282=plains
296=plains
394=plains
1758=forest
1759=plains
1760=forest
1761=plains
1762=plains
1763=plains
1764=plains
1765=forest
1766=plains
1767=plains
1768=plains
1779=plains
1780=forest
1783=plains
1789=plains

#Middle Luna
109=farmlands
329=plains
587=plains

#Upper Luna
298=farmlands
299=hills
327=farmlands
898=forest
1769=farmlands
1770=plains
1771=forest
1772=forest
1774=forest
1778=forest
1785=plains

#K_Carneter

#Carneter
19=hills
22=hills
30=forest
57=hills
1010=hills
1011=forest
1051=forest
1057=hills
1058=forest
1148=hills

#K Perlsedge

#Pearlsedge
14=forest
42=forest
43=farmlands
46=plains
943=plains
944=plains
945=plains
1052=forest
1053=forest
1056=hills

#Pearlywine
41=hills
49=hills
946=hills
947=hills
948=hills
949=hills
1054=forest
1055=hills

#K_Tretun

#Tretun
47=plains
48=forest
950=forest
951=hills
1001=plains

#Roilsard
32=hills
33=plains
34=forest
40=plains
59=plains
1002=plains
1003=forest
1004=plains
1005=forest
1006=forest
1007=forest
1008=forest
1009=hills
1094=plains

#E_Lencenor

#K_Rubyhold

#Rubyhold
62=dwarven_hold
64=mountains
65=mountains
1230=mountains
1231=mountains
1232=dwarven_hold

#K_Sorncost

#Coruan
84=plains
86=hills
89=plains
1076=hills
1077=hills
1078=plains

#Sormanni Hills
91=hills
100=hills
1079=plains
1080=forest
1081=hills
1082=plains
1083=forest

#Sorncost
85=plains
88=plains
90=hills
99=hills
1084=hills
1085=hills
1086=forest
1087=hills
1088=hills
1089=hills
1090=forest
1091=hills

#K_Deranne

#Deranne
110=forest
111=plains
112=plains
113=plains
121=plains
1137=forest
1138=forest
1139=plains
1140=plains
1141=plains
1142=plains
1143=plains
1144=forest
1145=plains
1146=plains
1147=plains

#Darom
58=farmlands
102=forest
104=plains
1126=forest
1127=plains
1128=forest
1129=plains
1130=plains
1131=plains
1132=forest
1133=forest
1134=forest
1135=forest
1136=forest

#K_Lorent

#Lorentaine
67=farmlands
69=mountains
70=plains
1014=plains
1015=forest
1021=forest
1022=forest
1023=forest
1024=mountains
1025=plains
1040=farmlands
1096=plains

#Lorenith
61=forest
68=forest
1013=forest
1016=plains
1018=forest
1064=forest

#Ainethan
38=plains
72=forest
73=plains
1017=hills
1019=plains
1020=forest
1075=hills
1092=plains
1093=hills
1095=forest

#Upper Bloodwine
74=farmlands
95=hills
108=plains
114=farmlands
1026=plains
1027=plains
1028=forest
1029=forest
1030=forest
1031=hills
1032=plains
1065=forest
1066=plains
1067=plains
1068=plains
1072=forest

#Lower Bloodwine
79=forest
80=plains
81=plains
97=forest
101=plains
1033=forest
1034=forest
1035=hills
1036=forest
1037=plains
1038=plains
1069=forest
1070=plains
1071=plains
1073=plains
1097=forest

#Enteben
96=plains
98=hills
1039=hills
1074=hills
1106=plains
1107=plains
1108=forest
1109=plains
1110=plains
1111=plains

#Horsegarden
78=plains
105=plains
1118=plains
1119=plains
1120=plains
1121=plains
1122=plains
1123=plains
1124=plains
1125=plains

#Crovania
82=plains
83=farmlands
106=plains
1112=plains
1113=hills
1114=plains
1115=plains
1116=hills
1117=hills

#Venail
93=plains
94=plains
127=plains
1098=forest
1099=forest
1100=forest
1101=forest
1102=forest
1103=plains
1104=forest
1105=plains

#Great Ording
75=plains
92=plains
107=plains
1149=plains
1150=plains
1151=plains
1152=plains
1153=plains
1154=plains
1155=plains
1156=plains
1157=plains

#Rewanwood
77=plains
87=plains
119=plains
1158=plains
1159=plains
1160=plains
1161=plains
1162=plains
1163=forest
1164=plains

#Redglades
115=forest
116=forest
117=forest
118=forest
120=forest
1165=forest
1166=forest
1167=forest
1168=forest
1169=forest
1170=forest
1171=forest
1172=forest

#Rosefield
71=mountains
133=plains
152=mountains
158=plains
1174=plains
1175=forest
1176=plains
1177=plains
1178=plains
1179=plains
1180=farmlands
1181=plains
1182=plains

#E_Small_Country(not in)

#K_Roysfort

#Roysfort
130=plains
131=plains
132=plains
1329=plains
1330=plains
1331=farmlands
1333=forest
1334=plains
1335=plains
1336=plains

#Bigwheat
134=forest
136=plains
1249=plains
1273=plains
1283=forest
1288=forest
1318=plains
1328=plains
1332=forest

#K_Ciderfield

#Appleton
137=plains
139=plains
163=farmlands
1241=plains
1242=forest
1243=forest
1244=plains
1246=farmlands
1247=plains
1248=farmlands

#Pearview
103=plains
147=hills
165=plains
1233=hills
1234=hills
1235=hills
1236=hills
1237=farmlands
1238=plains
1239=hills
1240=hills

#K_Viswall

#Thomsbridge
135=farmlands
159=farmlands
166=plains
1217=farmlands
1218=plains
1219=plains
1220=plains
1221=plains
1222=farmlands
1223=plains
1224=plains
1225=plains
1226=plains
1229=farmlands

#Barrowshire
160=hills
161=hills
162=plains
164=farmlands
1212=hills
1214=hills
1215=hills
1216=hills
1227=plains
1228=plains

#Greymill
12=plains
76=forest
149=plains
1202=forest
1203=plains
1204=forest
1205=forest
1206=forest
1207=plains
1208=farmlands

#Viswall
63=hills
66=farmlands
1213=farmlands

#K_Elkmarch

#Elkmarch
7=forest
206=forest
210=forest
348=forest
1193=forest
1194=forest
1195=forest
1196=forest
1197=forest
1198=forest
1199=forest
1200=forest

#Uelaire
207=plains
208=forest
209=plains
347=plains
1186=forest
1187=plains
1188=plains
1189=plains
1190=plains
1191=plains
1192=plains
1201=plains

#K_Beepeck

#Beepeck
60=wetlands
151=farmlands
155=wetlands
157=forest
205=plains
1183=forest
1184=plains
1185=plains
1209=plains
1210=farmlands
1211=plains
1989=farmlands
1990=farmlands

#K_Iochand

#Southroy
123=plains
124=plains
125=plains
128=plains
1245=plains
1337=plains
1338=plains
1339=plains
1340=plains
1341=plains
1430=plains
1432=plains

#Portnamm
126=farmlands

#Iochand
129=plains
141=forest
142=plains
143=plains
1342=plains
1352=plains
1354=plains
1355=plains
1356=plains
1357=plains
1358=plains

#K_Reveria

#Reaver Coast
122=plains
138=plains
140=plains
1359=forest
1363=plains
1368=plains
1390=forest	
1391=plains
1393=plains

#Gnomish Pass
144=plains
145=plains
148=mountains
167=hills
1394=plains
1395=plains
1396=plains
1397=forest
1401=plains
1403=plains
1404=plains
1405=hills
1406=forest

#E_Dragon_Coast

#K_Nimscodd

#Nimscodd
146=hills
169=hills
1398=hills
1399=hills
1400=hills

#Storm Isles
188=hills
190=hills
198=hills
199=hills

#K_Adderodd

#Adderodd
180=hills
182=hills
184=hills

#Royodann
183=mountains
187=hills

#K_Oddansbay

#Norcamb
178=forest
179=hills

#Oddansbay
175=forest
176=plains
177=farmlands

#K_Sildnamm

#Sildnamm
173=hills
174=farmlands

#Widdechand
170=hills
171=hills
172=farmlands
181=farmlands

#K_Mallazex

#Kobildzex
185=mountains
186=mountains
4646=mountains

#Soxun Kobildzex
189=mountains
4643=mountains
4644=mountains
4645=mountains

#E_Alenor

#Coddoran

#Coddoran
197=hills
200=hills
694=plains

#K_Eaglecrest

#Dragonhills
150=mountains
211=hills
213=hills
1407=hills
1409=hills
1410=hills
1412=hills
1420=hills

#Fluddhill
212=hills
214=wetlands
215=wetlands
336=hills
1421=forest
1422=plains
1424=wetlands
1425=hills
1426=plains
1427=hills
1428=wetlands
1429=wetlands

#K_Westmoors

#Westmoor
191=hills
192=wetlands
193=wetlands
194=wetlands
345=wetlands
1433=wetlands
1434=hills
1435=hills
1438=wetlands
1439=wetlands
1440=hills
1445=wetlands
1448=wetlands
1449=hills
1450=wetlands
1451=hills
1452=hills
1453=wetlands
1489=hills
1490=wetlands
1491=wetlands
1492=wetlands

#Moorhills
195=hills
196=hills
1446=hills
1447=wetlands
1454=hills
1455=hills
1456=hills

#Beronmoor
201=hills
202=wetlands
203=wetlands
204=wetlands
1457=wetlands
1458=wetlands
1459=wetlands
1460=hills
1461=wetlands
1462=wetlands
1463=wetlands
1464=wetlands
1465=wetlands
1466=plains
1469=wetlands
1470=wetlands
1481=wetlands
1483=wetlands
1484=wetlands
1485=hills
1486=plains
1487=wetlands
1488=wetlands

#K_Adshaw

#Serpentguard
697=hills
698=hills
699=plains
2659=hills
2660=plains
2661=taiga
2662=taiga
2663=plains
2664=plains

#Celmaldor
695=plains
696=taiga
2665=hills
2666=taiga
2667=taiga
2668=hills
2669=mountains

#Adderwood
700=plains
703=taiga
724=plains
2670=taiga
2671=hills
2672=hills
2673=plains
2674=plains
2675=taiga
2676=taiga

#Bayvic
705=hills
723=hills
2682=plains
2683=hills
2684=hills
2685=plains
2686=plains

#Adshaw
702=taiga
731=taiga
752=taiga
2677=taiga
2678=taiga
2679=taiga
2680=taiga
2681=plains

#West_Chillsbay
704=hills
706=taiga
721=plains
722=taiga
2687=taiga
2688=hills
2689=hills
2690=taiga
2691=plains
2692=plains
2693=plains

#K_Gawed

#Westmounts
217=hills
218=plains
340=hills
1965=plains
1966=plains
1967=plains
1968=hills
1969=hills
1970=hills
1971=plains

#Ginfield
220=forest
337=forest
339=plains
1959=forest
1960=plains
1961=plains
1962=plains
1963=forest
1964=forest

#Vertesk
216=farmlands
219=plains
235=plains
251=plains
1423=plains
1948=plains
1949=plains
1950=farmlands
1951=plains
1952=plains
1953=plains
1954=plains
1955=plains
1956=plains
1957=plains
1958=forest

#Gawed
221=plains
222=plains
223=taiga
341=hills
346=hills
1972=plains
1973=plains
1974=plains
1978=plains
1979=taiga
1981=taiga
1982=hills
1983=hills
1984=hills
1985=hills
1986=taiga
1987=taiga
1988=taiga

#Greatmarch
225=plains
226=hills
240=taiga
2017=taiga
2018=taiga
2019=taiga
2020=taiga
2021=taiga
2022=plains
2023=hills
2024=hills
2025=hills
2027=hills
2031=taiga
2032=hills

#Balvord
227=hills
228=taiga
241=hills
2028=taiga
2029=taiga
2030=taiga
2033=hills
2034=taiga

#Alenic Expanse
224=plains
237=plains
239=plains
243=plains
338=plains
1991=plains
1992=plains
1993=plains
1994=plains
1995=plains
1996=plains
1997=plains
1998=plains
1999=plains
2000=plains
2001=plains
2002=plains
2003=plains
2004=plains
2005=plains
2006=plains
2007=plains
2008=plains
2009=plains

#Oudescker
242=taiga
343=plains
344=taiga
2010=plains
2011=taiga
2012=taiga
2013=taiga
2014=taiga
2015=taiga
2016=taiga

#Arbaran
238=forest
244=forest
245=forest
246=forest
247=forest
252=forest
256=forest
349=forest
2055=forest
2056=forest
2057=forest
2058=forest
2059=forest
2060=forest
2061=forest
2062=forest
2063=forest
2064=forest
2065=forest
2066=forest
2067=forest
2068=forest
2069=forest
2070=forest
2071=forest
2072=forest
2073=forest
2074=forest
2075=forest
2076=plains
2077=forest

#E_Businor

#K_Busilar

#Lorin
374=hills
378=plains
1633=mountains
1634=hills
1635=hills
1636=hills
1637=hills
1638=plains

#Busilari Straits
39=plains
372=plains
373=plains
1650=plains
1651=plains
1653=plains
1662=plains
1663=plains
1664=plains
1665=plains
1669=plains
1670=plains

#Lioncost
367=hills
368=plains
1657=hills
1658=hills
1659=hills
1660=plains
1661=plains
1671=plains
1672=plains
1673=plains
1674=plains

#Hapiande
408=plains
1692=hills
1693=plains
1694=hills
1695=hills
1696=hills
1697=plains
1698=plains
1699=plains

#Lorbet
365=hills
366=hills
1675=hills
1676=hills
1677=hills
1678=hills
1680=hills

#Stonehold
362=plains
363=forest
1687=forest
1688=forest
1689=hills
1690=forest
1691=hills

#Hortosare
371=plains
1681=plains
1682=hills
1683=hills
1684=forest
1685=forest
1686=hills

#Canno
370=hills
1640=hills
1641=hills
1642=hills
1643=plains
1646=hills
1647=plains
1654=plains
1655=plains
1656=mountains

#K_Eborthil

#Eborthil
369=hills
380=hills
1622=hills
1623=hills
1624=hills
1625=hills
1626=hills
1639=hills

#Tefkora
35=hills
36=hills
379=hills
1627=hills
1628=hills
1629=hills
1630=hills
1631=hills
1632=hills

#E_Castanor

#K_Ibevar 

#Ibevar
352=forest
353=forest
356=hills
359=forest
1705=forest
1706=forest
1707=forest
1708=forest
1709=mountains
1710=forest
1711=forest
1712=hills
1713=forest
1714=forest
1715=forest
1716=forest
1721=forest

#Iarthan
354=hills
361=forest
422=mountains
1717=mountains
1718=forest
1719=forest
1720=forest
1725=mountains

#Cursewood
248=forest
257=forest
355=forest
358=forest
2239=forest
2240=forest
2241=forest
2242=forest
2243=forest
2244=forest
2245=forest
2246=forest

#Whistlevale
775=plains
777=hills
778=hills
779=mountains
2229=hills
2230=hills
2231=hills
2232=hills
2233=hills
2234=hills
2235=plains
2236=plains

#Nurael
261=hills
350=mountains
351=forest
360=forest
1700=hills
1701=forest
1702=forest
1703=forest
1704=forest

#K_Bal_Mire

#Middle Alen
710=taiga
725=hills
726=taiga
2487=taiga
2488=taiga
2489=taiga
2490=taiga

#Bal Mire
229=wetlands
230=wetlands
231=wetlands
232=wetlands
2447=wetlands
2448=wetlands
2449=wetlands
2450=wetlands
2451=wetlands
2452=wetlands
2453=wetlands
2454=wetlands
2455=wetlands
2456=wetlands
2457=wetlands
2458=wetlands
2459=wetlands
2460=wetlands

#Falsemire
759=hills
761=hills
762=plains
2461=plains
2462=hills
2463=wetlands
2464=hills
2465=plains
2466=hills
2467=plains

#K_Agrador

#Cannwood
727=hills
728=forest
750=hills
751=hills
753=hills
758=hills
2469=hills
2470=hills
2471=hills
2472=hills
2473=hills
2474=forest
2475=forest
2481=hills
2482=hills
2483=hills
2484=forest
2485=hills
2486=hills

#Agradalen
729=taiga
749=taiga
2468=hills
2476=hills
2477=taiga
2478=taiga
2479=taiga
2480=taiga
2491=taiga
2492=hills
2493=hills
2494=hills
2495=taiga
2496=taiga

#K_Vrorenmarch

#East_Chillsbay
701=taiga
707=plains
709=plains
712=plains
713=taiga
2649=plains
2650=plains
2651=taiga
2652=taiga
2653=plains
2654=taiga
2655=plains
2656=plains
2657=plains
2658=plains

#Sondar
714=taiga
715=taiga
2641=taiga
2642=taiga
2643=taiga
2644=taiga

#Cedesck
717=plains
718=plains
719=hills
720=hills
2630=plains
2631=plains
2632=taiga
2633=plains
2634=hills
2635=taiga
2636=plains
2637=plains
2638=taiga
2639=taiga
2640=taiga

#Wudhall
711=taiga
716=hills
2645=taiga
2646=taiga
2647=taiga
2648=taiga

#Vrorenwall
735=plains
736=taiga
737=taiga
738=taiga
739=hills
740=taiga
2623=hills
2624=hills
2625=taiga
2626=taiga
2627=taiga
2628=taiga
2629=taiga

#Tencfor
730=taiga
732=mountains
2611=taiga
2612=hills
2613=mountains
2614=hills

#Ebonmarck
741=taiga
742=hills
744=taiga
745=taiga
748=taiga
2615=taiga
2616=taiga
2617=hills
2618=taiga
2619=hills
2620=taiga
2621=taiga
2622=taiga

#K_Adenica

#Adenica
763=hills
764=plains
765=hills
792=farmlands
2267=plains
2268=hills
2269=plains
2270=plains
2271=farmlands
2272=hills
2273=plains
2274=hills
2275=forest

#Acenguard
756=farmlands
757=farmlands
760=hills
767=hills
2276=hills
2277=forest
2278=hills
2279=plains
2280=farmlands
2281=plains
2282=plains
2283=hills
2284=hills

#Rohibon
768=plains
769=plains
770=plains
771=plains
2247=plains
2248=plains
2249=plains
2250=plains
2251=plains
2252=plains
2253=plains
2254=plains
2255=plains

#Taran Plains
772=plains
773=plains
774=hills
2256=plains
2257=hills
2258=plains
2259=plains
2260=plains
2261=forest
2262=plains
2263=forest

#Valefort
776=mountains
780=mountains
2237=mountains
2238=hills

#Verteben
781=farmlands
782=hills
2264=hills
2265=forest
2266=mountains

#K_Merewood

#Northmerewood
766=forest
783=forest
788=forest
790=forest
2285=plains
2286=forest
2287=forest
2288=forest
2289=forest
2290=forest
2291=forest
2292=forest

#Merescker
784=hills
785=forest
786=forest
787=forest
789=forest
2293=forest
2294=forest
2295=forest
2296=forest
2297=plains
2298=forest
2299=hills
2300=hills
2301=hills
2302=plains

#Oudmerewood
793=plains
794=forest
795=hills
796=forest
2303=forest
2304=forest
2305=plains
2306=plains
2307=plains
2308=plains

#K_Devaced

#Devaced
791=hills
797=forest
798=farmlands
799=plains
2316=hills
2317=plains
2318=hills
2319=plains
2320=plains
2321=plains
2322=plains
2323=plains

#Vernham
800=forest
801=plains
808=forest
809=plains
810=forest
2324=plains
2325=hills
2326=plains
2327=plains
2328=plains
2329=plains
2330=plains
2331=plains
2332=plains
2333=plains
2335=plains
2336=plains

#Dostans Way
802=hills
803=forest
804=forest
805=forest
807=forest
2309=forest
2310=plains
2311=hills
2312=forest
2313=forest
2314=forest
2315=forest

#K_Cast

#Northmere
755=plains
835=farmlands
836=plains
2417=plains
2418=farmlands
2419=plains
2420=plains
2421=hills
2422=farmlands
2423=plains

#Nath
837=farmlands
839=farmlands
2439=farmlands
2440=plains
2441=plains
2442=farmlands
2443=farmlands
2444=farmlands
2497=forest
2502=forest

#Westgate
747=taiga
754=farmlands
842=forest
848=hills #taiga
2432=taiga
2433=taiga
2434=forest
2435=forest
2436=taiga
2437=taiga
2438=taiga

#Northyl
743=hills
746=taiga
849=hills
850=hills
2410=taiga
2411=mountains
2412=taiga
2413=hills
2414=taiga
2415=taiga
2416=hills

#Trialmount
840=mountains
843=plains
845=taiga
847=taiga
2445=forest
2446=hills
2499=farmlands
2500=hills
2501=hills
2510=forest

#Serpentsmarck
846=taiga
852=mountains
858=mountains
859=taiga
2498=taiga
2503=taiga
2504=hills
2505=hills
2506=hills
2507=hills
2508=hills
2509=taiga

#The South Citadel
834=farmlands
2424=plains
2427=plains
2428=plains
2429=farmlands
2430=plains
2431=plains

#K_Castonath

#Castonath
831=farmlands
832=farmlands
833=farmlands
2511=farmlands
2512=farmlands
2513=farmlands
2514=farmlands
2515=farmlands

#K_Anor

#Southgate
828=hills #forest
830=farmlands
2403=plains
2516=farmlands
2517=farmlands
2518=farmlands
2519=hills
2520=forest

#Foarhal
841=plains
853=forest
855=forest
2521=farmlands
2522=hills
2523=plains
2524=forest
2525=farmlands
2526=hills
2527=forest
2528=forest

#Oldhaven
866=forest
873=forest
874=hills
2535=forest
2536=forest
2537=forest
2538=forest
2539=forest
2540=forest

#Bradsecker
844=hills
851=plains
854=hills
2529=taiga
2530=hills
2531=hills
2532=hills
2533=farmlands
2534=farmlands

#Ardent Glade
864=hills
865=hills
869=hills
2541=plains
2542=plains
2543=hills
2544=hills
2545=hills
2546=taiga
2547=hills
2548=hills
2549=plains
2550=hills

#Steelhyl
860=mountains
862=hills #less steep
2551=hills
2552=taiga
2553=mountains
2554=mountains

#K_Sarwick

#Kondunn
861=hills
863=hills
868=hills
870=hills #taiga
2555=hills
2556=hills
2557=hills
2558=hills
2559=hills
2560=forest
2561=hills
2562=hills

#Sarwood
872=hills
878=forest
879=forest
2571=hills
2572=hills
2573=hills
2574=hills
2575=forest
2576=forest
2577=forest
2578=hills

#Nortessord
867=hills
871=mountains
881=forest
2563=mountains
2564=plains
2565=hills
2566=forest
2567=forest
2568=forest
2569=forest
2570=forest

#Esshyl
880=hills
882=hills
884=hills
2579=hills
2580=forest
2581=mountains
2582=mountains
2583=plains
2584=mountains

#K_Humacfeld

#Burnoll
821=farmlands
823=hills
825=hills
2377=farmlands
2378=farmlands
2379=plains
2380=plains
2381=farmlands
2384=hills
2385=hills
2386=plains
2387=hills
2388=plains
2389=hills

#Themin
820=hills
822=hills
2375=plains
2376=plains
2382=hills
2383=hills

#Humacvord
824=hills
826=hills
827=farmlands
856=farmlands
857=plains
2390=hills
2391=farmlands
2392=farmlands
2393=farmlands
2394=hills
2395=hills
2396=hills
2397=hills
2398=farmlands
2399=farmlands
2400=plains
2401=plains
2402=plains

#Wallor
829=farmlands
2404=farmlands
2405=plains
2406=farmlands
2407=plains
2408=forest
2409=forest

#K_Blademarches

#Clovenwood
806=forest
811=forest
815=forest
2337=forest
2338=forest
2339=forest
2340=forest

#Beastgrave
812=forest
813=forest
814=hills
2341=forest
2342=hills
2343=hills
2344=forest

#Blademarch
816=farmlands
817=hills
818=farmlands
819=hills
2345=hills
2346=farmlands
2347=farmlands
2348=plains
2349=hills
2350=farmlands
2351=farmlands
2352=hills
2353=farmlands
2354=plains
2355=hills
2356=hills
2357=farmlands

#Medirleigh
875=hills
876=farmlands
877=hills
885=farmlands
2362=plains
2363=farmlands
2364=plains
2365=hills
2366=farmlands
2367=hills
2368=hills
2369=forest
2370=plains
2371=forest
2372=hills
2373=hills
2374=hills

#Swapstroke
886=plains
887=plains
2358=hills
2359=hills
2360=plains
2361=plains

#K_Marrhold

#Dryadsdale
883=hills
888=forest
891=forest
2585=hills
2586=hills
2587=hills
2588=farmlands
2589=forest
2590=forest
2591=mountains
2592=mountains

#Marrhold
895=mountains
896=hills
897=mountains
4097=mountains
2601=hills
2602=mountains
2603=hills
2604=mountains
2605=mountains
2606=mountains
2607=hills
2609=mountains
2610=mountains

#Hornwood
889=plains
890=plains
892=hills
2593=plains
2594=plains
2595=hills
2596=forest

#Doewood
893=hills
894=mountains
2597=forest
2598=forest
2599=hills
2600=mountains

#E_Dostanor

#K_Corvuria

#Tiferben
423=hills
425=hills
426=plains
427=plains
428=plains
430=plains
2182=hills
2183=hills
2184=hills
2185=hills
2186=plains
2187=hills
2188=plains
2189=plains
2190=plains
2191=plains
2192=plains
2193=plains
2195=plains

#Blackwoods
429=hills
435=forest
436=forest
437=forest
2210=mountains
2211=hills
2212=mountains
2213=forest
2214=mountains
2215=hills
2216=hills
2217=hills
2218=hills
2219=forest

#Ravenhill
431=hills
432=plains
433=forest
434=forest
2196=hills
2197=hills
2198=plains
2199=plains
2201=plains
2202=forest
2203=plains
2204=forest
2205=forest
2206=plains
2207=hills
2208=plains
2209=forest

#Bal Dostan
438=hills
439=forest
440=forest
441=hills
2220=hills
2221=hills
2222=hills
2223=hills
2224=forest
2225=forest
2226=hills
2227=forest
2228=hills

#K_Corveld

#Dreadmire
442=wetlands
443=wetlands
444=wetlands
446=wetlands
450=wetlands

#Corveld
445=wetlands
447=wetlands
448=wetlands
449=wetlands
451=wetlands

#K_Ourdia

#Lencmark
506=plains
507=forest
508=forest

#Tencmark
509=forest
510=hills
511=hills

#Ourdmark
512=hills
513=mountains
514=hills
515=hills

#E_Gerudia

#K_Jotuntar

#Norjotuntar
987=taiga
988=taiga
989=taiga
2705=wetlands
2710=taiga
989=taiga
2709=taiga
2708=taiga
2704=taiga
2706=taiga
987=wetlands
2707=taiga

#Sudjotuntar
990=taiga
991=taiga
992=taiga
2700=taiga
2699=mountains
992=mountains
2702=taiga
2703=taiga
2701=taiga

#K_Urviksten

#Esfjall
984=mountains
986=mountains
2711=mountains
2712=mountains
2715=hills
2714=mountains
2713=mountains

#Naugsvol
981=mountains
982=taiga
983=taiga
2716=mountains
2717=mountains
2718=taiga
2719=mountains

#Avnkaup
978=wetlands
979=mountains
980=taiga
2720=mountains
2721=taiga
2722=taiga
2723=mountains
2724=mountains

#K_Rimurhals

#Jotunhamr
985=hills
2766=hills
2767=hills

#Olavslund
975=taiga
976=mountains
977=taiga
2725=taiga
2726=taiga
977=plains
975=plains
2727=plains
2730=taiga
2728=taiga
2729=mountains
2732=hills
2731=hills

#Drekkiskali
972=mountains
973=mountains
974=mountains
2733=mountains
2734=mountains
2735=mountains
2736=mountains
2737=mountains

#K_Bjarnik

#Bifrutja
708=hills
952=hills
953=hills
2761=hills
2760=plains
2759=hills
2762=hills
2765=hills
2764=wetlands
2763=plains

#Bjarnland
954=plains
955=wetlands
956=plains
2738=mountains
2739=mountains
2741=plains
2740=hills
2742=plains
2743=plains
2744=wetlands
2745=taiga
2746=taiga

#Alptborg
957=taiga
964=taiga
2782=plains
2783=taiga
2784=taiga
2787=mountains
2786=mountains
2785=taiga

#Siadett
965=taiga
966=wetlands
967=taiga
2789=taiga
2788=taiga
2790=taiga
2791=taiga
2792=taiga
2794=taiga
2793=hills

#Ismark
968=taiga
970=taiga
971=taiga
2802=mountains
2803=taiga
2801=taiga
2800=taiga
2796=taiga
2795=taiga
2798=taiga
2799=taiga
2797=mountains

#Revrland
959=plains
960=plains
993=plains
997=plains
2749=wetlands
2747=plains
2748=plains
2752=hills
2754=plains
2750=plains
2751=wetlands
2755=plains
2756=plains
2757=taiga
2757=mountains
2753=wetlands
2758=mountains

#Haugmyrr
961=taiga
962=hills
2769=plains
2770=taiga
2768=taiga
2772=mountains
2771=wetlands
2773=hills

#Sarbann
958=taiga
963=mountains
2777=taiga
2776=taiga
2778=taiga
2775=taiga
2774=taiga

#Kaldrland
969=taiga
998=taiga
2779=taiga
2780=mountains
2781=taiga

#K_Obrtroll

#Dalrfjall
994=mountains
995=mountains
1000=mountains

#Thurrsbol
996=mountains
999=hills

#E_Kheterata

#K_Ehka

#Ekha
386=drylands
387=drylands
388=hills
402=drylands
407=hills

#Akarat
389=hills
390=desert_mountains
391=desert_mountains
392=desert_mountains
393=desert_mountains

#K_Khasa

#Khasa
383=hills
384=hills
385=hills
400=drylands
406=hills

#Ikasakan
395=desert_mountains
396=desert_mountains
398=desert_mountains
401=desert_mountains

#K_Deshak

#Deshak
381=hills
382=drylands
403=drylands
404=drylands
405=hills

#Hapak
397=desert_mountains
399=desert_mountains
453=desert
458=desert

#K_Ayshanaz

#Krahkeysa
6091=desert
6092=desert
6093=desert
6094=desert
6095=desert

#Axakmoz
6096=oasis
6097=desert
6098=desert
6099=desert

#Boozazn
459=desert
460=desert
461=desert

#K_Kheterata

#Ohitsopot
454=desert
455=desert
456=desert
457=desert

#Sopotremit
462=floodplains
463=floodplains
503=floodplains
546=floodplains

#Aakheta
464=floodplains
466=floodplains
467=floodplains
468=floodplains

#Anarat
469=floodplains
471=floodplains
6088=floodplains

#Nirat
472=desert
473=floodplains
491=desert

#Khetarat
474=floodplains
475=floodplains
502=desert
592=desert
6089=desert

#Golkora 
476=desert
477=desert
478=desert
479=floodplains

#Hittputiushesh
465=desert
470=desert
490=desert

#Ibtat Axast
492=desert
493=desert
495=desert
497=desert

#Awaashesh
504=desert
505=desert

#Masusopot
498=desert
499=desert
500=desert
501=desert

#Ibtatu
494=drylands
496=drylands

#K_Irsmahap

#Hapmot
480=desert
481=desert
482=desert
6090=desert

#Dawimshesh
483=drylands
484=drylands
486=drylands
487=drylands
5676=drylands

#Miugesh
5478=drylands
5479=drylands
5480=drylands

#E_Bulwar

#K_Re_Uyel

#Crathanor
452=hills
516=hills
517=mountains
518=mountains
519=mountains

#Re_Uyel
521=hills
2851=hills
2836=hills
2837=hills
2838=plains
2839=hills

#Azka-szel-Azka
522=hills
2840=hills
2841=hills
2842=hills

#Zorntanas
523=hills
2843=hills
581=hills

#Akrad Til
520=hills
2848=mountains
2849=mountains
2850=plains

#Kaproya Telen
528=forest
2844=forest
2845=forest

#Azka Barzil
529=mountains
2846=plains
2847=mountains

#K_Ovdal_Tungr

#Ovdal_Tungr
525=mountains
526=dwarven_hold
527=mountains
2855=dwarven_hold
2856=dwarven_hold
2857=mountains
2858=hills

#K_Bahar

#Vernat
530=forest
2859=mountains

#Eduz-Wez
531=forest
2860=forest
2861=hills

#Bazibar
534=mountains
2864=mountains

#Agshelum
535=forest
2862=forest
2863=forest

#Pir Ainathil

533=hills

#Aqatbar
532=desert_mountains
536=forest
537=forest
541=forest
544=desert_mountains

#Azka_Evran
538=forest
539=desert_mountains
540=desert_mountains

#Azkasad
649=desert_mountains
650=desert_mountains

#Birsartenslib
542=hills
543=forest
549=forest

#Bahar Szel-Uak
545=desert_mountains
547=forest
548=forest
550=forest

#K_Gelkalis

#Eludasul
652=desert_mountains
653=hills
654=hills

#Gelkarzan
655=desert_mountains
656=drylands

#Gelkalis
664=hills
665=desert_mountains

#K_Firanyalen

#Hranapas
668=desert_mountains
669=desert_mountains
670=desert
671=desert_mountains
4117=desert_mountains

#Uvolate
672=hills
673=hills
674=hills

#Firanyalen
675=hills
676=hills
677=mountains
678=mountains

#K_Harpylen

#Creylore
666=desert_mountains
667=desert_mountains

#Harpylen
660=desert_mountains
661=desert_mountains
663=desert_mountains

#Alyzksaan
651=desert_mountains
657=desert_mountains
658=desert_mountains
659=desert_mountains
662=desert_mountains

#K_Imuluses
551=desert_mountains
552=desert_mountains
553=desert_mountains
554=desert_mountains
555=desert_mountains
556=desert_mountains
557=desert_mountains

#K_Idanas
559=drylands
560=floodplains
586=floodplains
588=floodplains
593=drylands
594=drylands
595=drylands

#K_Brasan
561=floodplains
562=drylands
563=floodplains
564=floodplains
565=floodplains
567=floodplains
568=drylands
570=desert
572=floodplains
584=desert_mountains

#K_Drolas
573=hills
574=hills
577=hills
578=hills
580=hills
582=hills

#K_Sad_Sur

679=desert_mountains
680=desert_mountains
682=desert_mountains

597=desert_mountains
683=desert_mountains
685=desert_mountains

686=desert_mountains
692=desert_mountains
693=desert_mountains

688=desert_mountains
689=desert_mountains

687=desert_mountains
691=desert_mountains

#K_Kumarses
589=desert_mountains
590=drylands
591=floodplains
603=drylands
608=floodplains
609=drylands
610=drylands
614=drylands

#K_Bulwar
596=floodplains
598=floodplains
599=floodplains
600=floodplains	
601=floodplains
602=floodplains
604=floodplains
605=desert
606=desert
607=floodplains
611=drylands
612=floodplains
625=floodplains
626=floodplains
627=floodplains
690=desert

#Akalzes
613=floodplains
615=drylands
616=floodplains
617=desert_mountains
618=desert_mountains
619=desert_mountains
620=drylands
621=drylands
622=drylands
623=drylands
624=drylands
628=drylands

#avamezan
629=drylands
630=drylands
632=drylands
633=drylands

#Hasr
631=drylands
634=drylands
635=drylands
636=drylands
637=drylands
638=desert
639=drylands
640=drylands
641=drylands

#Azka-Sur
558=desert_mountains
585=desert_mountains
642=drylands
643=drylands
644=desert
645=desert_mountains
646=desert_mountains
647=desert_mountains
648=desert

#Seghdihr
4124=dwarven_hold_surface

#E_Salahad

#Harragrar
485=desert
488=desert
489=desert
571=desert
583=desert
681=desert
684=desert
6100=desert
6101=desert
6102=desert
6103=desert
6104=desert

#Krahwix
5461=desert
5462=drylands
5463=drylands
5464=drylands
5465=desert
5472=drylands
5475=desert
5476=drylands
5477=drylands
5532=drylands
5533=desert

#Dasmazar
566=desert_mountains
5431=desert
5432=desert
5433=desert
5434=desert
5435=desert
5435=desert
5667=desert
5668=desert

#Apasih
2910=desert_mountains
2911=desert
2912=desert
2913=desert_mountains
2923=desert
2924=desert
2925=desert
2926=desert
2927=desert
2928=desert
2929=desert
2930=desert
4400=desert

#Siadanlen
2900=desert
2901=desert
2902=desert
2903=desert_mountains
2904=desert_mountains
2905=desert
2906=desert_mountains
2907=desert
2908=desert_mountains
2909=desert_mountains
2914=dwarven_hold_surface
2915=desert
2916=desert
2917=desert_mountains
2918=desert_mountains
2919=desert
2920=desert
2921=desert_mountains
2922=desert_mountains
4376=desert_mountains