#k_azka_sur
##d_azka_sur
###c_azka_sur
643 = {		#Azka-Sur

    # Misc
    culture = surani
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_sardazar
642 = {		#Sardazar

    # Misc
    culture = surani
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_saranaz
644 = {		#Saranaz

    # Misc
    culture = surani
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_sarsad
647 = {		#SarSad

    # Misc
    culture = surani
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_jikarzax
###c_jikarzax
585 = {		#Jikarzax

    # Misc
    culture = masnsih
    religion = old_bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_zanagu
558 = {		#Zanagu

    # Misc
    culture = masnsih
    religion = old_bulwari_sun_cult
	holding = tribal_holding

    # History
}

##d_nabilsu
###c_nabilsu
645 = {		#Nabilsu

    # Misc
    culture = surani
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_irrkornaz
648 = {		#Irrkornaz

    # Misc
    culture = surani
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_xharnax
646 = {

    # Misc
    culture = surani
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}